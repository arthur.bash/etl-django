import mmap
from datetime import date, datetime
from typing import Optional

from dateutil.parser import parse as dateutil_parse
from django.utils import timezone

#from validate_email import validate_email_or_fail


import logging



def get_num_lines(file_path) -> int:
    fp = open(file_path, "r+")
    buf = mmap.mmap(fp.fileno(), 0)
    lines = 0
    while buf.readline():
        lines += 1
    return lines


def parse_date(date: str) -> Optional[date]:
    if date:
        try:
            return dateutil_parse(date).astimezone(timezone.get_default_timezone()).date()
        except ValueError:
            return None
    else:
        return None


def parse_datetime(date: str) -> Optional[datetime]:
    if date:
        try:
            return dateutil_parse(date).astimezone(timezone.get_default_timezone())
        except ValueError:
            return None
    else:
        return None


def parse_bool(value: str) -> bool:
    return value == 'True'

def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.pdf', '.doc', '.docx', '.jpg', '.png', '.xlsx', '.xls', '.csv']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension.')

def validate_xls(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.xls']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension xls.')

def validate_xlsx(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.xlsx']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension xlsx.')

def validate_xls_xlsx(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.xlsx','.xls']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension xlsx or xls.')



# def parse_email(email: str) -> str:
#     if email:
#         try:
#             is_valid = validate_email_or_fail(email_address=email, check_regex=True,
#                                               check_mx=False, use_blacklist=False, debug=False)
#         except:
#             is_valid = False
#         if not is_valid:
#             email = ''
#     else:
#         email = ''

#     return email
