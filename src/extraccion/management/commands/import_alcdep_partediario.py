
import os
import pandas as pd
import numpy as np
from tqdm import tqdm
from src.pd_alcdep.helpers import (
    Test_pd_alcdep
)
from src.pd_alcdep.models import AlDepParteDiarioFileOriginal
from django.core.management import BaseCommand
class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument("file_path")
        parser.add_argument("--fuerza_update",default=False)

    def handle(self, *args, **options):
        file_path,  update = options["file_path"], options["fuerza_update"]
        files = os.listdir(file_path)
        existe = lambda fecha_reporte: AlDepParteDiarioFileOriginal.objects.filter(fecha_reporte=fecha_reporte).exists()

        name_files = []
        archivos_fallados = []
        for filename in tqdm(files, total=len(files)):
            if '.~' in filename:
                continue
            else:
                name_files.append(filename)
                

                file = file_path + "/" + filename
                try:
                    df = pd.read_excel(file,sheet_name= 'Parte Diario')
                except: 
                    df = pd.read_excel(file)


                
                test_completo = Test_pd_alcdep(df,filename)
                procesador = test_completo.testear()

                fecha_reporte = test_completo.get_report_date()
                if test_completo.successful():
                    if ( not existe(fecha_reporte) or update):
                        obj_file = AlDepParteDiarioFileOriginal.objects.create(
                        fecha_reporte = fecha_reporte,
                        nombre = filename,
                        flag_date = True,
                        flag_tabla1 = True,
                        flag_tabla2 = True,
                        flag_tabla3 = True,

                        t1 = test_completo.get_t1(),
                        t2 = test_completo.get_t2(),
                        t3 = test_completo.get_t3()
                        )
                        
                        procesador.process_file_partediario_varios(obj_file)
                        procesador.process_file_partediario_cupo_SPB(obj_file)
                        procesador.process_file_partediario_mix_semana(obj_file)

                else:
                    print(f"No se proceso: filename {filename} ---> {test_completo}")
                    archivos_fallados.append(filename)
    
        print(f" Archivos NO cargados ----> {archivos_fallados}")

