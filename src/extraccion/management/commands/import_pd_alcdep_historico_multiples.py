import csv
from django.core.management import BaseCommand
from tqdm import tqdm
from src.utils.import_helpers import get_num_lines, parse_date

from src.people.models import Genero
from src.juditial_params.models import SituacionJudicial
from src.spb_params.models import Destino
from src.pd_alcdep.models import AlDepParteDiarioPoblacion, AlDepParteDiarioMultiplesDim



class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument("csv_file")
        parser.add_argument("--delimiter", default=",")
        parser.add_argument("--ignore_update",default=False)
        parser.add_argument("--has-header", default=False, const=True, nargs="?")

    def handle(self, *args, **options):
        csv_file_path, delimiter, ignore_update = options["csv_file"], options["delimiter"], options['ignore_update']
        with open(csv_file_path, newline="") as csv_file:
            with open('logs/import_pd_alcdep_historico_multiples.log', 'w') as log:
                reader = csv.reader(csv_file, delimiter=delimiter, quotechar='"')
                lines = get_num_lines(csv_file_path)
                if options["has_header"]:
                    next(reader)  # Skip header
                    lines = lines - 1
                
               

                for row in tqdm(reader, total=lines):
                    fecha_reporte, destino_legacy_id, destino_txt, sjuridica_legacy_id, sjuridica_txt, genero_legacy_id, capacidad, poblacion_presente, situacion_juridica_cantidad, ingresos_pendientes, translados_pendientes, libertad, ocupados, disponibles, translados_pendientes_txt, detalle  = row
                    
                    # print('#####################################')
                    # print('PD Multiples')

                    fecha_reporte = parse_date(fecha_reporte)
                    # print('fecha_reporte',fecha_reporte)

                    destino_legacy_id = int(destino_legacy_id or 0)
                    destino = Destino.objects.get(legacy_id=destino_legacy_id)
                    # print('destino',destino)

                    genero_legacy_id = int(genero_legacy_id or 0)
                    genero = Genero.objects.get(legacy_id=genero_legacy_id)

                    capacidad = int(capacidad or 0)
                    #total_pob = int(total_pob or 0) # TODO generar dato
                    poblacion_presente = int(poblacion_presente or 0)
                    ingresos_pendientes = int(ingresos_pendientes or 0)
                    translados_pendientes = int(translados_pendientes or 0)
                    libertad = int(libertad or 0)
                    ocupados = int(ocupados or 0)
                    translados_pendientes_txt = translados_pendientes_txt.strip()
                    detalle = detalle.strip()

                    #To handle Null values in column
                    try:
                        disponibles = int(disponibles)
                    except:
                        disponibles = 0
                    
                    # print('sjuridica',sjuridica)
                    # print('genero',genero)
                    # print('destino',capacidad)

                    # print('genero_legacy_id',genero_legacy_id)

                    ####### INIT AlDepParteDiarioMultiplesDim ####################

                    exist_and_ignore = lambda find_date,ignore: bool(ignore) &  AlDepParteDiarioMultiplesDim.objects.filter(fecha_reporte=find_date,destino = destino, genero = genero ).exists() 

                    if( (genero_legacy_id == 1) and (not exist_and_ignore(fecha_reporte,ignore_update)) ):
                        # print('Guado multiples dim de Masculino')
                            AlDepParteDiarioMultiplesDim.objects.update_or_create(
                                fecha_reporte = fecha_reporte,
                                destino = destino,
                                genero =  genero,
                                defaults = {
                                    "capacidad" : capacidad,
                                    #"total" : total_pob,
                                    "poblacion_presente" : poblacion_presente,
                                    "ingresos_pendientes" : ingresos_pendientes,
                                    "translados_pendientes" : translados_pendientes,
                                    "libertad" : libertad,
                                    "ocupados" : ocupados,
                                    "disponibles" : disponibles,
                                    "translados_pendientes_txt" : translados_pendientes_txt,
                                    "detalle" : detalle
                                },
                            )
                    elif( (
                        (genero_legacy_id == 2) and (destino_legacy_id == 68)) or 
                        (genero_legacy_id == 3) and (destino_legacy_id == 66) ):
                        if not exist_and_ignore(fecha_reporte,ignore_update) : 
                        # print('Es F y pertenece a destino La Plata III o, es T y pertenece a destino Pettinato)')
                            AlDepParteDiarioMultiplesDim.objects.update_or_create(
                                fecha_reporte = fecha_reporte,
                                destino = destino,
                                genero =  genero,
                                defaults = {
                                    "capacidad" : capacidad,
                                    #"total" : total_pob,
                                    "poblacion_presente" : poblacion_presente,
                                    "ingresos_pendientes" : ingresos_pendientes,
                                    "translados_pendientes" : translados_pendientes,
                                    "libertad" : libertad,
                                    "ocupados" : ocupados,
                                    "disponibles" : disponibles,
                                    "translados_pendientes_txt" : translados_pendientes_txt,
                                    "detalle" : detalle
                                },
                            )
                    # else:
                    #     print('La fila con genero_legacy_id', genero_legacy_id ,'y con destino', destino_txt,'no tiene información.')

                    ######## FIN #################################################

                    ######## INIT AlDepParteDiarioPoblacion ######################
    
                    sjuridica_legacy_id = int(sjuridica_legacy_id or 0)
                    situacion_juridica = SituacionJudicial.objects.get(legacy_id=sjuridica_legacy_id)
                    situacion_juridica_cantidad = int(situacion_juridica_cantidad or 0)
                    exist_and_ignore_poblacion = lambda find_date,ignore: AlDepParteDiarioPoblacion.objects.filter(fecha_reporte=find_date,destino = destino, situacion_juridica = situacion_juridica ).exists() & bool(ignore) 
                   
                    if not exist_and_ignore_poblacion(fecha_reporte,ignore_update):
                        AlDepParteDiarioPoblacion.objects.update_or_create(
                            fecha_reporte = fecha_reporte,
                            destino = destino,
                            situacion_juridica =  situacion_juridica,
                            defaults = {
                                "cantidad" : situacion_juridica_cantidad,
                            },
                        )
