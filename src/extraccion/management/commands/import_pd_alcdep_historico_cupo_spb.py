import csv
from django.core.management import BaseCommand
from tqdm import tqdm
from src.utils.import_helpers import get_num_lines, parse_date

from src.people.models import Genero
from src.pd_alcdep.models import AlDepParteDiarioCupoSPB



class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument("csv_file")
        parser.add_argument("--delimiter", default=",")
        parser.add_argument("--ignore_update",default=False)
        parser.add_argument("--has-header", default=False, const=True, nargs="?")

    def handle(self, *args, **options):
        csv_file_path, delimiter, ignore_update= options["csv_file"], options["delimiter"],options['ignore_update']
        with open(csv_file_path, newline="") as csv_file:
            with open('logs/import_pd_alcdep_historico_cupo_spb.log', 'w') as log:
                reader = csv.reader(csv_file, delimiter=delimiter, quotechar='"')
                lines = get_num_lines(csv_file_path)
                if options["has_header"]:
                    next(reader)  # Skip header
                    lines = lines - 1
                
                gender_m = Genero.objects.get(legacy_id=1) # Masc
                gender_f = Genero.objects.get(legacy_id=2) # Fem
                gender_t = Genero.objects.get(legacy_id=3) # Trans
                
                #Funion que contempla si existe y se ignora la actualizacion correspondiente
                exist_and_ignore = lambda find_date,ignore: bool(ignore) & AlDepParteDiarioCupoSPB.objects.filter(fecha_reporte=find_date).exists()
                for row in tqdm(reader, total=lines):
                    fecha_reporte, quantity_m, quantity_f, quantity_t  = row
                    
                    fecha_reporte = parse_date(fecha_reporte)
                    
                    quantity_m = int(quantity_m or 0)
                    quantity_f = int(quantity_f or 0)
                    quantity_t = int(quantity_t or 0)

                    # print('#####################################')
                    # print('Cupos SPB')

                    # print('quantity_m',quantity_m)
                    # print('quantity_f',quantity_f)
                    # print('quantity_t',quantity_t)

                   
                    if not exist_and_ignore(fecha_reporte,ignore_update):
                        AlDepParteDiarioCupoSPB.objects.update_or_create(
                            fecha_reporte = fecha_reporte,
                            genero =  gender_m,
                            defaults = {
                                "cantidad" : quantity_m
                            },
                        )
                        AlDepParteDiarioCupoSPB.objects.update_or_create(
                            fecha_reporte = fecha_reporte,
                            genero =  gender_f,
                            defaults = {
                                "cantidad" : quantity_f
                            },
                        )
                        AlDepParteDiarioCupoSPB.objects.update_or_create(
                            fecha_reporte = fecha_reporte,
                            genero =  gender_t,
                            defaults = {
                                "cantidad" : quantity_t
                            },
                        )
