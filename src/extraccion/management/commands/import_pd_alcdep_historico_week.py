import csv

import pandas as pd
import numpy as np

from django.core.management import BaseCommand
from tqdm import tqdm

import datetime

from src.utils.import_helpers import get_num_lines, parse_date
from src.pd_alcdep.helpers import persist_ingresados, persist_carga_cop, persist_cupos_otorgados
from src.pd_alcdep.models import (AlDepParteDiarioCupoOtorgado, AlDepParteDiarioIngresados, AlDepParteDiarioCargaCop  )

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument("csv_file")
        parser.add_argument("--delimiter", default=",")
        parser.add_argument("--ignore_update",default=False)
        parser.add_argument("--has-header", default=False, const=True, nargs="?")

    def handle(self, *args, **options):
        csv_file_path, delimiter, ignore_update = options["csv_file"], options["delimiter"], options["ignore_update"]
        with open(csv_file_path, newline="") as csv_file:
            with open('logs/import_pd_alcdep_historico_week.log', 'w') as log:
                reader = csv.reader(csv_file, delimiter=delimiter, quotechar='"')
                lines = get_num_lines(csv_file_path)
                if options["has_header"]:
                    next(reader)  # Skip header
                    lines = lines - 1
                
                for row in tqdm(reader, total=lines):
                    interpreted_date, ingresos, cupos_acl, detenidos  = row
                    


                    # print('#####################################')
                    # print('Cupos ALC')

                    interpreted_date = pd.to_datetime(interpreted_date)
                    # print('interpreted_date', interpreted_date)
                    
                    ingresos = int(ingresos or 0) #interpreted_date = fecha_reporte -1
                    cupos_acl = int(cupos_acl or 0) #interpreted_date = fecha_reporte -1
                    detenidos = int(detenidos or 0) #interpreted_date = fecha_reporte

                    # print('ingresos',ingresos)
                    # print('cupos_acl',cupos_acl)
                    # print('detenidos',detenidos)

                    day_number = interpreted_date.day_of_week
                    # print('day_number ', day_number)

                    if day_number == 4:
                        fecha_reporte = interpreted_date + datetime.timedelta(days=3)
                    elif day_number == 0:
                        fecha_reporte = interpreted_date + datetime.timedelta(days=1)
                    elif day_number == 1:
                        fecha_reporte = interpreted_date + datetime.timedelta(days=1)
                    elif day_number == 2:
                        fecha_reporte = interpreted_date + datetime.timedelta(days=1)
                    elif day_number == 3:
                        fecha_reporte = interpreted_date + datetime.timedelta(days=1)
                    else:
                        print('TODO handle error detecting second date. Number of date error.')
                    
                    # print('fecha_reporte', fecha_reporte)

                    report_file = None

                    exist_and_ignore = lambda find_date,ignore,class_parte_diario: bool(ignore) &  class_parte_diario.objects.filter(fecha_reporte=find_date).exists() 

                    if not exist_and_ignore(fecha_reporte,ignore_update,AlDepParteDiarioIngresados):
                        persist_ingresados(fecha_reporte, interpreted_date, ingresos, report_file)
                    if not exist_and_ignore(fecha_reporte,ignore_update,AlDepParteDiarioCupoOtorgado):
                        persist_cupos_otorgados(fecha_reporte, interpreted_date, cupos_acl, report_file)
                    if not exist_and_ignore(fecha_reporte,ignore_update,AlDepParteDiarioCargaCop):
                        persist_carga_cop(interpreted_date, detenidos, report_file) # interpreted_date = fecha_reporte