from django.conf import settings
from django.db import models
from django.core.files import File
from datetime import date, datetime


from src.utils.import_helpers import validate_xlsx, validate_xls

from model_utils.models import TimeStampedModel
from simple_history.models import HistoricalRecords

import os

def get_upload_path(instance, filename):
    now = datetime.now()
    path = "{}/{}/{}/{}".format('Ex',now.year, now.month, now.day)
    return os.path.join(path, filename)



# ----------------------------------------

class ExtraccionFileOriginal(TimeStampedModel):
    fecha_reporte = models.DateField("fecha de reporte", null=True, blank=True)
    nombre = models.CharField("Nombre", max_length=255, blank=True)

    file_original = models.FileField(
        upload_to=get_upload_path,
        validators=[validate_xlsx],
        null=True,
        verbose_name="archivo original")

    flag = models.BooleanField("Flag Date", default=False)

    version = models.IntegerField("Version",default = 5)

    changed_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, blank=True, null=True)

    historial = HistoricalRecords()

    class Meta:
        verbose_name = "Archivo original Extraccion"
        verbose_name_plural = "Archivos originales Extraccion"
        ordering = ['-fecha_reporte']

    def save_file_original(self, nombre: str, file: str):
        self.file_original.save(nombre, File(open(file, "rb")))
        self.save()

    def __str__(self):
        return self.nombre

# ----------------------------------------

class Extraccion(TimeStampedModel):
    changed_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, blank=True, null=True)

    report_file = models.ForeignKey(ExtraccionFileOriginal, on_delete=models.PROTECT, verbose_name='archivo')
    cantidad = models.IntegerField("cantidad",default=0)

    historial = HistoricalRecords()

    class Meta:
        verbose_name = "Parte Diario extraccion"
        verbose_name_plural = "Parte Diario extraccion"
        ordering = ['-id']

    # def __str__(self):
    #     return str(self.fecha_reporte)

# ----------------------------------------
