from rest_framework import serializers

from src.extraccion.models import Extraccion,ExtraccionFileOriginal


class ExtraccionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Extraccion
        fields = [
            "id",
            "report_file",
            "cantidad"
        ]
