from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

import pandas as pd
import numpy as np

import logging
from src.utils.import_helpers import parse_date

from src.extraccion.models import ExtraccionFileOriginal,Extraccion
from src.utils.import_helpers import report_error

@receiver(pre_save, sender=ExtraccionFileOriginal)
def partediario_original_pre_save(sender, instance: ExtraccionFileOriginal, **kwargs):
    if not instance.id:  # created
        if instance.file_original:
            instance.nombre = instance.file_original.file.name
            with open('logs/extraccion.log', 'w') as log:

                # Read xls file InMemoryUploadedFile
                buffer = instance.file_original.file.read()
                # print (buffer)
                df = pd.read_excel(buffer)
                # test = Test_pd_alcdep(df,instance.nombre)
                
                # instance._procesador = test.testear()

@receiver(post_save, sender=ExtraccionFileOriginal)
def partediario_original_post_save(sender, instance: ExtraccionFileOriginal, created, **kwargs):
    if created:  # created

        if instance.file_original:

            with open('logs/extraccion.log', 'w') as log:

                # Read xls file InMemoryUploadedFile
                buffer = instance.file_original.file.read()
                df = pd.read_excel(buffer, header=0)


        else: # no hay archivo
            pass

    else:  # updated
        pass
