from django.shortcuts import render

from drf_rw_serializers import viewsets
from rest_framework.permissions import IsAuthenticated 
from rest_framework_api_key.permissions import HasAPIKey

from rest_framework.viewsets import ReadOnlyModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from src.extraccion.serializers import ExtraccionSerializer
from src.extraccion.models import Extraccion

class ExtraccionViewset(ReadOnlyModelViewSet):
    serializer_class = ExtraccionSerializer
    read_serializer_class = ExtraccionSerializer
    queryset = Extraccion.objects.all()
    permission_classes = []
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    # ordering = ['-fecha_reporte']
    # ordering_fields = ['id', 'fecha_reporte']
    # filterset_fields = {
    #     'id':['exact'],
    #     'situacion_juridica':['exact'],
    #     'fecha_reporte':['gte', 'lte', 'exact', 'gt', 'lt']
    # }

