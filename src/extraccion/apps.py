from django.apps import AppConfig


class Extraccion(AppConfig):
    name = 'src.extraccion'

    verbose_name = "Partes Diarios Extraidos"
    
    def ready(self):
        try:
            from . import signals  # noqa F401
        except ImportError:
            # print(traceback.format_exc())
            # print("no cargo el signal alcaidia")
            pass
