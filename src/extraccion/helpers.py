import pandas as pd
import numpy as np

import datetime

from django.db.models import Q, Sum
from  dateutil import rrule



import logging




class Test_pd_alcdep():
    """Esta clase se encarga de testear el documento de alcaldia y verificar su version,
        De este modo obtenemos 1 clase procesador que contiene 3 dataframe 
            correspondiente a las 3 tablas diferentes   indicadas en el parte diario   """
    def __init__(self,df,filename):
        self.df = df
       

    def testear(self):
        """Si la fecha se pudo identificar, armar los 3 dataframe"""
        self._test_date()
        if self._date:
            self._analizar_tabla1(self.df.copy())
            self._analizar_tabla2(self.df.copy())
            self._analizar_tabla3(self.df.copy())

            self._procesador.set_report_date(self._report_date)
            return self._procesador

        #print (f" version:  {self._version}   header =   {self._header}     footer =   {self._footer}    date =   {self._date}")


    def get_procesador(self):
        return self._procesador
