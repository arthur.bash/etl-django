from django.contrib import admin

from rangefilter.filter import DateRangeFilter
from simple_history.admin import SimpleHistoryAdmin
from tabular_export.admin import export_to_csv_action, export_to_excel_action

from django_admin_relation_links import AdminChangeLinksMixin

from admin_numeric_filter.admin import NumericFilterModelAdmin, SingleNumericFilter, RangeNumericFilter, SliderNumericFilter


from src.extraccion.models import ExtraccionFileOriginal,Extraccion

@admin.register(ExtraccionFileOriginal)
class AlDepParteDiarioFileOriginalAdmin(SimpleHistoryAdmin):
    list_display = [
        "id",
        "nombre",
        "fecha_reporte",
        "flag",
        "file_original",
        "created",
        "modified"
    ]
    list_filter = [
        ("fecha_reporte",DateRangeFilter),
        "flag",
        ("created",DateRangeFilter),
        ("modified",DateRangeFilter),
    ]
    search_fields = ["nombre"]
    readonly_fields = ("nombre", "fecha_reporte", "flag","changed_by")
    actions = (export_to_csv_action, export_to_excel_action)
    class Media:
        pass


