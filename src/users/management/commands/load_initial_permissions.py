# backend/management/commands/initgroups.py
from django.core.management import BaseCommand
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType

from src.extraccion.models import (
    ExtraccionFileOriginal,
)

GROUPS_PERMISSIONS = {
    'Extraccion Carga': {
        ExtraccionFileOriginal: ['view', 'add'],
    }
}


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)

    help = "Create default groups"

    def handle(self, *args, **options):
        # Loop groups
        for group_name in GROUPS_PERMISSIONS:

            # Get or create group
            group, _ = Group.objects.get_or_create(name=group_name)

            # Loop models in group
            for model_cls in GROUPS_PERMISSIONS[group_name]:
                ct = ContentType.objects.get_for_model(model_cls, for_concrete_model=False)

                # Loop permissions in group/model
                for perm_index, perm_name in \
                        enumerate(GROUPS_PERMISSIONS[group_name][model_cls]):

                    # Generate permission name as Django would generate it
                    codename = perm_name + "_" + model_cls._meta.model_name

                    try:
                        # Find permission object and add to group
                        perm = Permission.objects.get(codename=codename, content_type=ct)
                        group.permissions.add(perm)
                        self.stdout.write("Adding "
                                          + codename
                                          + " to group "
                                          + group.__str__())
                    except Permission.DoesNotExist:
                        try:
                            perm = Permission.objects.get(codename=perm_name)
                            group.permissions.add(perm)
                            self.stdout.write("Adding "
                                              + perm_name
                                              + " to group "
                                              + group.__str__())
                        except Permission.DoesNotExist:
                            self.stdout.write(perm_name + " not found")
