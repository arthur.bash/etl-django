from .base import *  # noqa
from .base import env


# GENERAL
# ------------------------------------------------------------------------------

DEBUG = True

# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = env(
    "DJANGO_SECRET_KEY",
    default="aSY0HGaNgqQ5WCwJsd0Jg8fjULI5LAO0q4T3wnsLAhR1j5krXTNFv7PzZXxqGWPs",
)
# https://docs.djangoproject.com/en/dev/ref/settings/#test-runner
TEST_RUNNER = "django.test.runner.DiscoverRunner"

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]

DATABASES = {
    'default': {
        'ENGINE': 'mysql.connector.django',
        'NAME': 'preproduccion',
        'USER': 'dfortini',
        'PASSWORD': 'mJus@33cia',
        'HOST': '10.25.1.16',
        'PORT': '3306',
        'CONN_MAX_AGE': 600000
    }
}

# mysql -u dfortini -p p78!hjkaA -h 10.25.1.16  mjuspoblacion
# mysql -u dfortini -p -h 10.25.1.16  mjuspoblacion
# https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ["localhost", "0.0.0.0", "127.0.0.1", "10.42.8.43", "10.42.8.28", "10.42.8.33"]

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        "LOCATION": "",
    }
}

# TEMPLATES
# ------------------------------------------------------------------------------
TEMPLATES[-1]["OPTIONS"]["loaders"] = [  # type: ignore[index] # noqa F405
    (
        "django.template.loaders.cached.Loader",
        [
            "django.template.loaders.filesystem.Loader",
            "django.template.loaders.app_directories.Loader",
        ],
    )
]

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = "django.core.mail.backends.locmem.EmailBackend"

# Your stuff...
# ------------------------------------------------------------------------------
INSTALLED_APPS += ["django_extensions"]  # noqa F405
