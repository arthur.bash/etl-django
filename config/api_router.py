from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from src.users.api.views import UserViewSet
from src.extraccion.views import ExtraccionViewset

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)

# People params
router.register("extraccion", ExtraccionViewset)



app_name = "api"
urlpatterns = router.urls
