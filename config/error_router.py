from src.extraccion.views import ExtraccionViewset
from django.urls import  path
def rutas_errores ():
    lista_rutas = [
        path('error/extraccion/', ExtraccionViewset.as_view({'get': 'list'}))
    ]
    return lista_rutas