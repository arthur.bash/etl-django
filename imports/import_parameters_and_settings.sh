red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

# echo ""
# echo "${green}[1/3] Creando configuración inicial..."
# echo "${reset}"
# python manage.py create_initial_config


echo ""
echo "${green}[1/13] Creando grupos de permisos..."
echo "${reset}"
python manage.py load_initial_permissions

echo ""
echo "${green}[2/13] Completando la tabla hash_date"
echo "${reset}"
python manage.py import_hash_date_new data/import_iniciales/hash_date_new.csv --has-header=True

echo ""
echo "${green}[3/13] Importando Departamentos y Dependencias Judiciales..."
echo "${reset}"
python manage.py import_dependencias_judiciales data/import_iniciales/ds_dptoJudicial_depciaJudicial.csv --has-header=True

echo ""
echo "${green}[4/13] Importando Situaciones judiciales..."
echo "${reset}"
python manage.py import_situaciones_judiciales data/import_iniciales/ds_sit_judicial.csv --has-header=True

echo ""
echo "${green}[5/13] Importando estados de los internos en las causas"
echo "${reset}"
python manage.py import_situaciones_juridicas data/import_iniciales/ds_estado_interno_causa.csv --has-header=True

echo ""
echo "${green}[6/13] Importando Dependencias COP..."
echo "${reset}"
python manage.py import_cop_params data/import_iniciales/ds_COP_params.csv --has-header=True


echo ""
echo "${green}[7/13] Importando Destinos..."
echo "${reset}"
python manage.py import_spb_destinos data/import_iniciales/ds_destinos.csv --has-header=True

echo ""
echo "${green}[8/13] Importando Tipos de Egreso..."
echo "${reset}"
python manage.py import_spb_tipos_de_egreso data/import_iniciales/ds_tipo_egreso.csv --has-header=True

echo ""
echo "${green}[9/13] Importando Géneros..."
echo "${reset}"
python manage.py import_people_generos data/import_iniciales/ds_persona_genero.csv --has-header=True


echo ""
echo "${green}[10/13] Importando Tipos de documentos..."
echo "${reset}"
python manage.py import_people_type_doc data/import_iniciales/ds_persona_tipo_doc.csv --has-header=True

echo ""
echo "${green}[11/13] Importando Nacionalidades..."
echo "${reset}"
python manage.py import_paises data/import_iniciales/ds_paises.csv --has-header=True

echo ""
echo "${green}[12/13] Importando Delitos..."
echo "${reset}"
python manage.py import_delitos data/import_iniciales/ds_delitos.xlsx 

echo ""
echo "${green}[13/13] Importando tabla de Equivalencias..."
echo "${reset}"
python manage.py import_equivalencias data/import_iniciales/equivalencias.csv --has-header=True


echo ""
echo "${red}IMPORTACIÓN DE PARAMETROS Y SETTINGS TERMINADA"
echo "${reset}"