#########################
ETL DJANGO - Documentación
#########################

.. MinJus DA ETL Process - Documentación

Procesamiento y Análisis de Datos del Arthur bash
==================================================================

Aplicación de Ciencia de Datos desarrollada en Django y Docker

.. image:: Linux-Logo.png
   :width: 100

.. Data Science app with Django and Docker

.. toctree::
   :maxdepth: 3
   :caption: Contenidos:

   about

   