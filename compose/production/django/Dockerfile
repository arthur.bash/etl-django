FROM python:3.9-slim-buster as python

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV BUILD_ENV=production

WORKDIR /app


# Install apt packages
RUN apt-get update && apt-get install --no-install-recommends -y \
  # dependencies for building Python packages
  build-essential \
  libpq-dev  \
  nano gcc \
  libcap2 libcap2-bin libpam-cap iputils-ping gettext \
  default-libmysqlclient-dev   python-mysqldb \
  python3-dev  graphviz  libgraphviz-dev  pkg-config

# Create Python Dependency and Sub-Dependency Wheels.

RUN addgroup --system django \
    && adduser --system --ingroup django django

# Install required system dependencies
RUN apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && rm -rf /var/lib/apt/lists/*

# Requirements are installed here to ensure they will be cached.
COPY ./requirements .


# use wheels to install python dependencies
RUN pip install  -r ${BUILD_ENV}.txt

COPY ./compose/production/django/entrypoint /entrypoint
RUN sed -i 's/\r$//g' /entrypoint
RUN chmod +x /entrypoint

COPY ./compose/production/django/start /start
RUN sed -i 's/\r$//g' /start
RUN chmod +x /start

COPY --chown=django:django . ./

# make django owner of the WORKDIR directory as well.
RUN chown django:django ./

USER django

# RUN pip install mysqlclient


ENTRYPOINT ["/entrypoint"]
