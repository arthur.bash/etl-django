FROM python:3.9-slim-buster as build

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1


# Install apt packages
RUN apt-get update && apt-get install --no-install-recommends -y \
  # dependencies for building Python packages
  build-essential \
  libpq-dev  \
  python3-dev  pkg-config

# Install required system dependencies
RUN apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /docs

# Requirements are installed here to ensure they will be cached.
COPY ./requirements ./requirements
# All imports needed for autodoc.
RUN pip install -r ./requirements/documentation.txt

COPY ./documentation .  

RUN make html 
# COPY ./compose/production/docs/start /start-docs
# RUN sed -i 's/\r$//g' /start-docs
# RUN chmod +x /start-docs

FROM nginx:alpine as server
#RUN rm -rf /usr/share/nginx/html/*
RUN apk add --no-cache tzdata
ENV TZ=America/Argentina/Buenos_Aires
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
COPY --chown=nginx --from=build /docs/_build/html /usr/share/nginx/html/docs/
